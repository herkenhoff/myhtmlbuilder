//
//  File.swift
//  
//
//  Created by Christian Herkenhoff on 25.04.20.
//

import Foundation

public extension Sequence where Element: HtmlRendable {
    
    func render() -> String {
        return self.map({$0.render()}).joined()
    }
    
}

extension String: HtmlRendable {
    
    var htmlElement: HtmlElement {
        return HtmlElementBase(tag: HtmlTag.span, value: self)
    }
    
    public func render() -> String {
        return self.htmlElement.render()
    }
    
}
