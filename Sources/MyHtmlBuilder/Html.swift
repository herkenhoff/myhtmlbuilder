//
//  Html.swift
//  MyHtmlBuilder
//
//  Created by Christian Herkenhoff on 02.05.20.
//

import Foundation

public class Html: HtmlRendable {
    
    public var head = HtmlHead()
    public var body = [HtmlElement]()
    
    public init(){
        
    }
    
    public func render() -> String {
        let renderedBody = HtmlBuilder.make(tag: HtmlTag.body, value: body.map({$0.render()}).joined())
        let htmlContent = "\(head.render())\(renderedBody)"
        
        return HtmlBuilder.make(tag: HtmlTag.html, value: htmlContent)
    }
    
}

public class HtmlHead: HtmlRendable {
    
    public var title = ""
    public var style = ""
    public var charset = ""
    public var metas = [HtmlTagElement]()
    public var links = [HtmlTagElement]()
    public var scripts = [String]()
    public var baseUrl: String?
    
    public func render() -> String {
        var headContent = ""
        if let url = self.baseUrl {
            headContent += HtmlBuilder.make(tag: HtmlTag.base, attributes: [
                HtmlAttribute(key: "href", value: url),
                HtmlAttribute(key: "target", value: "_blank")
            ])
        }
        if !self.charset.isEmpty {
            headContent += HtmlBuilder.make(tag: HtmlTag.meta, attributes: [HtmlAttribute(key: "charset", value: self.charset)])
        }
        if !self.metas.isEmpty {
            headContent += self.metas.render()
        }
        if !self.title.isEmpty {
            headContent += HtmlBuilder.make(tag: HtmlTag.title, value: self.title)
        }
        if !self.links.isEmpty {
            headContent += self.links.render()
        }
        if !self.scripts.isEmpty {
            headContent += HtmlBuilder.make(tag: HtmlTag.script, value: self.scripts.joined())
        }
        if !self.style.isEmpty {
            headContent +=  HtmlBuilder.make(tag: HtmlTag.style, value: self.style)
        }
        
        return HtmlBuilder.make(tag: HtmlTag.head, value: headContent)
    }
    
}
