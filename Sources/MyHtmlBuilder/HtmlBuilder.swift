//
//  File.swift
//  
//
//  Created by Christian Herkenhoff on 25.04.20.
//

import Foundation

public struct HtmlAttribute {
    
    public let key: String
    public var value: String?
    
    public init(key: String){
        self.key = key
    }
    
    public init(key: String, value: String){
        self.key = key
        self.value = value
    }
    
}

public class HtmlBuilder {
    
    public class func make(tag: String, value: String) -> String {
        return "<\(tag)>\(value)</\(tag)>"
    }
    
    public class func make(tag: String, value: String, class cssClass: String) -> String {
        let attribute = HtmlAttribute(key: "class", value: cssClass)
        return self.make(tag: tag, value: value, attributes: [attribute])
    }
    
    public class func make(tag: String, value: String, attributes:[HtmlAttribute]?) -> String {
        guard let attributes = attributes, !attributes.isEmpty else {
            return self.make(tag: tag, value: value)
        }
        
        let formattedAttributes = Self.formattedAttributes(attributes)
        return "<\(tag) \(formattedAttributes)>\(value)</\(tag)>"
    }
    
    public class func make(tag: String, attributes: [HtmlAttribute]) -> String {
        let formattedAttributes = Self.formattedAttributes(attributes)
        return "<\(tag) \(formattedAttributes)>"
    }
    
    private class func formattedAttributes(_ attributes: [HtmlAttribute]) -> String {
        return (attributes.map { (attribute) -> String in
            if let value = attribute.value {
                return "\(attribute.key)=\"\(value)\""
            }else{
                return "\(attribute.key)"
            }
        }).joined(separator: " ")
    }
}
