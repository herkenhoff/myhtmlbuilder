//
//  File.swift
//  
//
//  Created by Christian Herkenhoff on 25.04.20.
//

import Foundation

public protocol HtmlElement: HtmlRendable {
    
    var tag: String { get }
    var value: String { get }
    var attributes: [HtmlAttribute] { get set }
    
}

public class HtmlTagElement: HtmlRendable {
    
    public var tag: String
    public var attributes = [HtmlAttribute]()
    
    public init(tag: String){
        self.tag = tag
    }
    
    public init(tag: String, attributes: [HtmlAttribute]){
        self.tag = tag
        self.attributes = attributes
    }
    
    public func render() -> String {
        return HtmlBuilder.make(tag: self.tag, attributes: self.attributes)
    }
    
}

public class HtmlElementBase: HtmlElement {
    
    public var tag: String
    public var value: String
    public var attributes = [HtmlAttribute]()
    
    public init(tag: String, value: String){
        self.tag = tag
        self.value = value
    }
    
    public init(tag: String, value: String, className: String){
        self.tag = tag
        self.value = value
        self.attributes = [HtmlAttribute(key: "class", value: className)]
    }
    
    public init(tag: String, value: String, attributes: [HtmlAttribute]){
        self.tag = tag
        self.value = value
        self.attributes = attributes
    }
    
    func setAttribute(key: String, value: String){
        if let index = self.attributes.firstIndex(where: {$0.key == key}) {
            self.attributes[index] = HtmlAttribute(key: key, value: value)
        }else{
            self.attributes.append(HtmlAttribute(key: key, value: value))
        }
    }
    
    func removeAttribute(_ key: String){
        self.attributes.removeAll(where: {$0.key == key})
    }
    
    public func render() -> String {
        if self.attributes.isEmpty {
            return HtmlBuilder.make(tag: tag, value: value)
        }
        return HtmlBuilder.make(tag: tag, value: value, attributes: attributes)
    }
}

public class HtmlContainerElement: HtmlElementBase {
    
    public var elements = [HtmlRendable]()
    
    public convenience init(tag: String){
        self.init(tag: tag, elements: [])
    }
    
    public init(tag: String, elements: [HtmlElement]) {
        super.init(tag: tag, value: "")
        self.elements = elements
    }
    
    public override func render() -> String {
        return HtmlBuilder.make(tag: self.tag, value: elements.map({$0.render()}).joined(), attributes: self.attributes)
    }
}

public class HtmlPlainTextElement: HtmlElementBase {
    
    public init(value: String){
        super.init(tag: HtmlTag.span, value: value)
    }
    
}

public class HtmlTextElement: HtmlElementBase {
    
    public init(value: String){
        super.init(tag: HtmlTag.p, value: value)
    }
    
}

public class HtmlTextAreaElement: HtmlElementBase {
    
    public var rows: Int?
    public var cols: Int?
    
    public init(value: String){
        super.init(tag: HtmlTag.textarea, value: value)
    }
        
    public override func render() -> String {
        if let r = self.rows {
            setAttribute(key: "rows", value: "\(r)")
        }
        if let c = self.cols {
            setAttribute(key: "cols", value: "\(c)")
        }
        return super.render()
    }
    
}

public class HtmlAElement: HtmlElementBase {
    
    public init(url: String, text: String) {
        super.init(tag: HtmlTag.a, value: text)
        self.attributes = [HtmlAttribute(key: "href", value: url)]
    }
    
    public init(url: String, text: String, openInNewWindows: Bool) {
        super.init(tag: HtmlTag.a, value: text)
        self.attributes = [HtmlAttribute(key: "href", value: url)]
        if openInNewWindows { self.attributes.append(HtmlAttribute(key: "target", value: "_blank"))}
    }
    
    public init(url: String, text: String, downloadFileName: String){
        super.init(tag: HtmlTag.a, value: text)
        self.attributes = [
            HtmlAttribute(key: "href", value: url),
            HtmlAttribute(key: "download", value: downloadFileName)
        ]
    }
    
}

public class HtmlButtonElement: HtmlElementBase {
    
    public var disabled = false {
        didSet {
            if disabled {
                self.attributes.append(HtmlAttribute(key: "disabled"))
            }else {
                self.attributes.removeAll(where: {$0.key == "disabled"})
            }
        }
    }
    
    public init(buttonText: String) {
        super.init(tag: HtmlTag.button, value: buttonText)
        self.attributes = [HtmlAttribute(key: "type", value: "button")]
    }
    
}

public class HtmlFrameElement: HtmlElementBase {
    
    public init(source: String) {
        super.init(tag: HtmlTag.iframe, value: "")
        self.attributes = [HtmlAttribute(key: "src", value: source)]
    }
    
}
