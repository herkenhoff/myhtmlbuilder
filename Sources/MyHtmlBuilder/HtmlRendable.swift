//
//  File.swift
//  
//
//  Created by Christian Herkenhoff on 25.04.20.
//

import Foundation

public protocol HtmlRendable {
    func render() -> String
}
