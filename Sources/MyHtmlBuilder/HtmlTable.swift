//
//  File.swift
//  
//
//  Created by Christian Herkenhoff on 25.04.20.
//

import Foundation

public class HtmlTable: HtmlTagElement {
    
    public var rows = [HtmlTableRow]()
    
    public init(){
        super.init(tag: HtmlTag.table)
    }
    
    public override func render() -> String {
        return HtmlBuilder.make(tag: self.tag, value: rows.render(), attributes: attributes)
    }
}

public class HtmlTableExtended: HtmlTable {
    
    public var tableHeader: HtmlTableHeader?
    public var tableBody = HtmlTableBody()
    public var tableFooter: HtmlTableFooter?
    
    public override func render() -> String {
        var renderedTable = ""
        if let renderedHeader = tableHeader?.render() {
            renderedTable += renderedHeader
        }
        renderedTable += tableBody.render()
        if let renderedFooter = tableFooter?.render() {
            renderedTable += renderedFooter
        }
        
        return HtmlBuilder.make(tag: self.tag, value: renderedTable, attributes: attributes)
    }
}

public class HtmlTableContainer: HtmlTagElement {
    
    var rows = [HtmlTableRow]()
    
    override init(tag: String){
        super.init(tag: tag)
    }
    
    convenience init(tag: String, rows: [HtmlTableRow]){
        self.init(tag: tag)
        self.rows = rows
    }
    
    public override func render() -> String {
        return HtmlBuilder.make(tag: self.tag, value: rows.render(), attributes: self.attributes)
    }
    
}

public class HtmlTableHeader: HtmlTableContainer {
    
    public convenience init(){
        self.init(rows: [])
    }
    
    public convenience init(rows: [HtmlTableRow]){
        self.init(tag: HtmlTag.thead, rows: rows)
    }
    
}

public class HtmlTableBody: HtmlTableContainer {
    
    public convenience init(){
        self.init(rows: [])
    }
    
    public convenience init(rows: [HtmlTableRow]){
        self.init(tag: HtmlTag.tbody, rows: rows)
    }
}

public class HtmlTableFooter: HtmlTableContainer {
    public convenience init(){
        self.init(rows: [])
    }
    
    public convenience init(rows: [HtmlTableRow]){
        self.init(tag: HtmlTag.tfoot, rows: rows)
    }
}

public class HtmlTableRow: HtmlTagElement {
    
    public var cells = [HtmlTableCellBase]()
    
    public convenience init() {
        self.init(cells: [])
    }
    
    public init(cells: [HtmlTableCellBase]) {
        super.init(tag: HtmlTag.tr)
        self.cells = cells
    }
    
    public override func render() -> String {
        return HtmlBuilder.make(tag: self.tag, value: cells.render(), attributes: self.attributes)
    }
}

public class HtmlTableCellBase: HtmlElementBase {
    
    public var colspan: Int?
    public var rowspan: Int?
    public var headers: String?
    
    public func addingColspan(_ colspan: Int) -> HtmlTableCellBase {
        self.colspan = colspan
        return self
    }
    
    public func addingRowspan(_ rowspan: Int) -> HtmlTableCellBase {
        self.rowspan = rowspan
        return self
    }
    
    public func addingHeaders(_ headers: String) -> HtmlTableCellBase {
        self.headers = headers
        return self
    }
    
    public override func render() -> String {
        if let c = self.colspan {
            setAttribute(key: "colspan", value: "\(c)")
        }
        if let r = self.rowspan {
            setAttribute(key: "rowspan", value: "\(r)")
        }
        if let h = self.headers {
            setAttribute(key: "headers", value: h)
        }
        return super.render()
    }
    
}

public class HtmlTableCell: HtmlTableCellBase {
    
    public init(value: String){
        super.init(tag: HtmlTag.td, value: value)
    }
    
    public init(value: String, className: String){
        super.init(tag: HtmlTag.td, value: value, className: className)
    }
    
    public init(value: String, attributes: [HtmlAttribute]){
        super.init(tag: HtmlTag.td, value: value, attributes: attributes)
    }
    
}

public class HtmlTableHeadCell: HtmlTableCellBase {
    
    public init(value: String){
        super.init(tag: HtmlTag.th, value: value)
    }
    
    public init(value: String, className: String){
        super.init(tag: HtmlTag.th, value: value, className: className)
    }
    
    public init(value: String, attributes: [HtmlAttribute]){
        super.init(tag: HtmlTag.th, value: value, attributes: attributes)
    }
    
}
