//
//  File.swift
//  
//
//  Created by Christian Herkenhoff on 02.05.20.
//  Source: https://www.mediaevent.de/html/html5-tags.html
//

import Foundation

public struct HtmlTag {
    
    //MARK: Base
    public static let doctype = "!DOCTYPE"
    public static let html = "html"
    public static let head = "head"
    public static let title = "title"
    public static let base = "base"
    public static let link = "link"
    public static let meta = "meta"
    public static let style = "style"
    
    //MARK: Section
    public static let body = "body"
    public static let article = "article"
    public static let section = "section"
    public static let nav = "nav"
    public static let aside = "aside"
    public static let hgroup = "hgroup"
    public static let header = "header"
    public static let footer = "footer"
    public static let address = "address"
    
    //MARK: Headline
    public static let h1 = "h1"
    public static let h2 = "h2"
    public static let h3 = "h3"
    public static let h4 = "h4"
    public static let h5 = "h5"
    public static let h6 = "h6"
    
    //MARK: Content
    public static let div = "div"
    public static let p = "p"
    public static let hr = "hr"
    public static let pre = "pre"
    public static let blockquote = "blockquote"
    public static let ol = "ol"
    public static let ul = "ul"
    public static let li = "li"
    public static let dl = "dl"
    public static let dt = "dt"
    public static let dd = "dd"
    public static let figure = "figure"
    public static let main = "main"
    
    //MARK: Text
    public static let a  = "a"
    public static let em = "em"
    public static let i = "i"
    public static let strong = "strong"
    public static let b = "b"
    public static let small = "small"
    public static let cite = "cite"
    public static let q = "q"
    public static let dfn = "dfn"
    public static let abbr = "abbr"
    public static let data = "data"
    public static let time = "time"
    public static let code = "code"
    public static let _var = "var"
    public static let samp = "samp"
    public static let kbd = "kbd"
    public static let sub = "sub"
    public static let sup = "sup"
    public static let u = "u"
    public static let mark = "mark"
    public static let ruby = "ruby"
    public static let rt = "rt"
    public static let rp = "rp"
    public static let rb = "rb"
    public static let rtc = "rtc"
    public static let  bdi = "bdi"
    public static let bdo = "bdo"
    public static let span = "span"
    public static let  br = "br"
    public static let wbr = "wbr"
    
    //MARK: Document
    public static let del = "del"
    public static let ins = "ins"
    public static let s = "s"
    
    //MARK: Media
    public static let img = "img"
    public static let picture = "picture"
    public static let iframe = "iframe"
    public static let embed = "embed"
    public static let object = "object"
    public static let param = "param"
    public static let video = "video"
    public static let audio = "audio"
    public static let source = "source"
    public static let track = "track"
    public static let area_map = "area map"
    public static let svg = "svg"
    public static let mathml = "mathml"
    
    //MARK: Table
    public static let table = "table"
    public static let caption = "caption"
    public static let col = "col"
    public static let colgroup = "colgroup"
    public static let tbody = "tbody"
    public static let thead = "thead"
    public static let tfoot = "tfoot"
    public static let td = "td"
    public static let th = "th"
    public static let tr = "tr"
    
    //MARK: Formular
    public static let form = "form"
    public static let fieldset = "fieldset"
    public static let label = "label"
    public static let legend = "legend"
    public static let input = "input"
    public static let datalist = "datalist"
    public static let button = "button"
    public static let select = "select"
    public static let option = "option"
    public static let optgroup = "optgroup"
    public static let textarea = "textarea"
    public static let output = "output"
    public static let progress = "progress"
    public static let meter = "meter"
    public static let keygen = "keygen"
    
    //MARK: Script
    public static let noscript = "nosccript"
    public static let script = "script"
    public static let canvas = "canvas"
    public static let template = "template"
    
    //MARK: Interactive
    public static let detail = "detail"
    public static let summery = "summery"
    public static let dialog = "dialog"
    
}
