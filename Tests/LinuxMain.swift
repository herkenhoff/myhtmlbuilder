import XCTest

import MyHtmlBuilderTests

var tests = [XCTestCaseEntry]()
tests += MyHtmlBuilderTests.allTests()
XCTMain(tests)
