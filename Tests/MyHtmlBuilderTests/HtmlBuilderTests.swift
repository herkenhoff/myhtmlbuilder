import XCTest
@testable import MyHtmlBuilder

final class HtmlBuilderTests: XCTestCase {
    
    func testHtmlTag(){
        XCTAssertEqual("html", HtmlTag.html)
    }
    
    func testMakeTag(){
        var tag = HtmlBuilder.make(tag: HtmlTag.p, value: "Hello World")
        var expectedTag = "<p>Hello World</p>"
        
        XCTAssertEqual(expectedTag, tag)
        
        tag = HtmlBuilder.make(tag: HtmlTag.p, value: "Hello World", class: "block")
        expectedTag = "<p class=\"block\">Hello World</p>"
        
        XCTAssertEqual(expectedTag, tag)
        
        tag = HtmlBuilder.make(tag: HtmlTag.a, value: "google", attributes: [
            HtmlAttribute(key: "href", value: "https://google.de"),
            HtmlAttribute(key: "target", value: "_blank")
        ])
        expectedTag = "<a href=\"https://google.de\" target=\"_blank\">google</a>"
        
        XCTAssertEqual(expectedTag, tag)
        
        var meta = HtmlBuilder.make(tag: HtmlTag.meta, attributes: [HtmlAttribute(key: "charset", value: "UTF-8")])
        var expectedMeta = "<meta charset=\"UTF-8\">"
        XCTAssertEqual(expectedMeta, meta)
        
        meta = HtmlBuilder.make(tag: HtmlTag.meta, attributes: [
            HtmlAttribute(key: "name", value: "description"),
            HtmlAttribute(key: "content", value: "Some content")
        ])
        expectedMeta = "<meta name=\"description\" content=\"Some content\">"
        XCTAssertEqual(expectedMeta, meta)
    }
    
    func testHtmlDivElement(){
        let divTag = HtmlContainerElement(tag: HtmlTag.div, elements: [
            HtmlElementBase(tag: HtmlTag.h1, value: "Title")
        ])
        let expected = "<div><h1>Title</h1></div>"
        XCTAssertEqual(expected, divTag.render())
    }
    
    func testHtmlButtonElement(){
        let button = HtmlButtonElement(buttonText: "Test")
        var expected = "<button type=\"button\">Test</button>"
        
        XCTAssertEqual(expected, button.render())
        
        button.disabled = true
        expected = "<button type=\"button\" disabled>Test</button>"
        
        XCTAssertEqual(expected, button.render())
    }
    
    func testHtmlRender(){
        let html = Html()
        html.head.title = "Test"
        html.head.charset = "UTF-8"
        html.body.append(HtmlTextElement(value: "Hello World"))
        html.body.append(HtmlContainerElement(tag: HtmlTag.div, elements: [
            HtmlElementBase(tag: HtmlTag.h1, value: "Title")
        ]))
        
        let renderedHtml = html.render()
        let expectedHtml = "<html><head><meta charset=\"UTF-8\"><title>Test</title></head><body><p>Hello World</p><div><h1>Title</h1></div></body></html>"
        
        XCTAssertEqual(expectedHtml, renderedHtml)
    }
    
    func testHtmlTextAreaTest(){
        let textarea = HtmlTextAreaElement(value: "Test")
        var expected = "<textarea>Test</textarea>"
        
        XCTAssertEqual(expected, textarea.render())
        
        textarea.cols = 3
        expected = "<textarea cols=\"3\">Test</textarea>"
        
        XCTAssertEqual(expected, textarea.render())
        
        textarea.rows = 2
        textarea.cols = 2
        expected = "<textarea cols=\"2\" rows=\"2\">Test</textarea>"
        
        XCTAssertEqual(expected, textarea.render())
    }
    
}
