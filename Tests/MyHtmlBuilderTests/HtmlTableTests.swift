import XCTest
@testable import MyHtmlBuilder

final class HtmlTableTest: XCTestCase {
    
    var row: HtmlTableRow {
        let row = HtmlTableRow()
        row.cells.append(HtmlTableCell(value: "Test"))
        return row
    }
    
    func testHtmlTable(){
        let table = HtmlTable()
        table.rows.append(self.row)
        
        let expected = "<table><tr><td>Test</td></tr></table>"
        
        XCTAssertEqual(expected, table.render())
    }
    
    func testHtmlTableExtended(){
        let table = HtmlTableExtended()
        table.tableHeader = HtmlTableHeader(rows: [self.row])
        table.tableBody = HtmlTableBody(rows: [self.row])
        table.tableFooter = HtmlTableFooter(rows: [self.row])
        
        let expected = "<table><thead><tr><td>Test</td></tr></thead><tbody><tr><td>Test</td></tr></tbody><tfoot><tr><td>Test</td></tr></tfoot></table>"
        
        XCTAssertEqual(expected, table.render())
    }
    
    func testHtmlTableHeader(){
        let header = HtmlTableHeader()
        header.rows.append(self.row)
        
        let expected = "<thead><tr><td>Test</td></tr></thead>"
        
        XCTAssertEqual(expected, header.render())
    }
    
    func testHtmlTableBody(){
        let body = HtmlTableBody()
        body.rows.append(self.row)
        
        let expected = "<tbody><tr><td>Test</td></tr></tbody>"
        
        XCTAssertEqual(expected, body.render())
    }
    
    func testHtmlTableFooter(){
        let footer = HtmlTableFooter()
        footer.rows.append(self.row)
        
        let expected = "<tfoot><tr><td>Test</td></tr></tfoot>"
        
        XCTAssertEqual(expected, footer.render())
    }
    
    func testHtmlTableRow(){
        let row = HtmlTableRow()
        row.cells.append(HtmlTableCell(value: "Test"))
        
        let expected = "<tr><td>Test</td></tr>"
        
        XCTAssertEqual(expected, row.render())
    }
    
    func testHtmlTableCell(){
        let cell = HtmlTableCell(value: "Test")
        var expected = "<td>Test</td>"
        
        XCTAssertEqual(expected, cell.render())
        
        let colspanCell = HtmlTableCell(value: "Test").addingColspan(2)
        expected = "<td colspan=\"2\">Test</td>"
        XCTAssertEqual(expected, colspanCell.render())
        
        let headCell = HtmlTableHeadCell(value: "Test")
        expected = "<th>Test</th>"
        XCTAssertEqual(expected, headCell.render())
        
        var row = HtmlTableRow(cells: [headCell])
        expected = "<tr><th>Test</th></tr>"
        XCTAssertEqual(expected, row.render())
        
        row = HtmlTableRow(cells: [cell])
        expected = "<tr><td>Test</td></tr>"
        XCTAssertEqual(expected, row.render())
    }
}
